
Basic commands:

.TL - Title
.AU - Author
.AI - Author's Institution
.AB - Abstract begin
.AE - Abstract End
.B - Bold-begin
.I - Italics-begin
.IP - Indented paragraph:
      Can be used as a macro to specify lists.
      Used as ordered, unordered lists

      .IP \[name\[tabwidth\]\]
.PP - Begin paragraph
.NH - Numbered heading

      .NH [Heading level]


Reference book:

- [Typing Documents on the UNIX System:
Using the – ms Macros with Troff and Nroff](https://www.troff.org/using-ms.pdf)
- `man 7 groff_ms`
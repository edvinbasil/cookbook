.TL
Update the read system call table using a rootkit.
.AU
Edvin Basil Samuval
Nikhil Nath R
Alan Thomas

.AI
National Institute of Technology, Calicut
.AB

The Linux kernel (and many other operating systems) can load kernel modules (e.g. device drivers) at runtime.
This allows us to insert a module that overrides kernel syscalls in order to add a middleware that writes to a file.
The rootkit can then pass the syscalls onto the kernel.
.AE
.PP

.nr step 1 1
When a system call (e.g. open() to open a file) is made by an application, the flow of control looks like this:
.IP \n[step] 3
An interrupt is triggered, and execution continues at the interrupt handler defined for that interrupt. On Linux, interrupt 80 is used.

A rootkit could replace the kernels interrupt handler by an own function. This requires a modification of the Interrupt Descriptor Table (IDT).[1]
.IP \n+[step]
The interrupt handler (named system_call() on Linux) looks up the address of the requested syscall in the syscall table, and executes a jump to the respective address.
A rootkit may 
    a) modify the interrupt handler to use a (rootkit-supplied) different syscall table, or 
    b) modify the entries in the syscall table to point to the rootkits replacement functions.

.IP \n+[step]
The syscall function is executed, and control returns to the application.

[1] Phrack issue 59, article 0x04 ("Handling the Interrupt Descriptor Table", by kad). 
